import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResizableV1Component } from './resizable-v1.component';

describe('ResizableV1Component', () => {
  let component: ResizableV1Component;
  let fixture: ComponentFixture<ResizableV1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResizableV1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResizableV1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
