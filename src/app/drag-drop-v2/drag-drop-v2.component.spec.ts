import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DragDropV2Component } from './drag-drop-v2.component';

describe('DragDropV2Component', () => {
  let component: DragDropV2Component;
  let fixture: ComponentFixture<DragDropV2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DragDropV2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DragDropV2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
