import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DragDropV1Component } from './drag-drop-v1.component';

describe('DragDropV1Component', () => {
  let component: DragDropV1Component;
  let fixture: ComponentFixture<DragDropV1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DragDropV1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DragDropV1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
