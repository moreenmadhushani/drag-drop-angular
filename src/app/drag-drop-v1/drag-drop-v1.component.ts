import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-drag-drop-v1',
  templateUrl: './drag-drop-v1.component.html',
  styleUrls: ['./drag-drop-v1.component.css']
})
export class DragDropV1Component implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  artists = [
    'Artist I - Davido',
    'Artist II - Wizkid',
    'Artist III - Burna Boy',
    'Artist IV - Kiss Daniel',
    'Artist V - Mayorkun',
    'Artist VI - Mr. Eazi',
    'Artist VII - Tiwa Savage',
    'Artist VIII - Blaqbonez',
    'Artist IX - Banky W',
    'Artist X - Yemi Alade',
    'Artist XI - Perruzi',
    'Artist XII - Seyi Shay',
    'Artist XIII - Teni'
  ];


}
