import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {DragDropModule} from '@angular/cdk/drag-drop'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DragDropV1Component } from './drag-drop-v1/drag-drop-v1.component';
import { DragDropV2Component } from './drag-drop-v2/drag-drop-v2.component';
import { ResizableV1Component } from './resizable-v1/resizable-v1.component';
import { ResizableModule } from 'angular-resizable-element';
import { SchedularComponent } from './schedular/schedular.component';
import { DragDropV3Component } from './drag-drop-v3/drag-drop-v3.component';

@NgModule({
  declarations: [
    AppComponent,
    DragDropV1Component,
    DragDropV2Component,
    ResizableV1Component,
    SchedularComponent,
    DragDropV3Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DragDropModule,
    ResizableModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { 

}
