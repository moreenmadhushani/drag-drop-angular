import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DragDropV1Component } from './drag-drop-v1/drag-drop-v1.component';
import { DragDropV2Component } from './drag-drop-v2/drag-drop-v2.component';
import { DragDropV3Component } from './drag-drop-v3/drag-drop-v3.component';
import { ResizableV1Component } from './resizable-v1/resizable-v1.component';
import { SchedularComponent } from './schedular/schedular.component';

const routes: Routes = [
  {
    path: 'v1',
    component: DragDropV1Component,
  },
  {
    path: 'v2',
    component: DragDropV2Component,
  },
  {
    path: 'v3',
    component: DragDropV3Component,
  },
  {
    path: 'rv1',
    component: ResizableV1Component,
  },
  {
    path: 'scheduler',
    component: SchedularComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
