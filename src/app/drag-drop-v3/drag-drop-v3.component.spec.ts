import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DragDropV3Component } from './drag-drop-v3.component';

describe('DragDropV3Component', () => {
  let component: DragDropV3Component;
  let fixture: ComponentFixture<DragDropV3Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DragDropV3Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DragDropV3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
