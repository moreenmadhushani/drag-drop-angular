import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-drag-drop-v3',
  templateUrl: './drag-drop-v3.component.html',
  styleUrls: ['./drag-drop-v3.component.css'],
})
export class DragDropV3Component implements OnInit {
  constructor() {}

  ngOnInit(): void {}

  dragStart(event) {
    event.dataTransfer.setData('Text', event.target.id);
    document.getElementById('demo').innerHTML = 'Started to drag the p element';
  }

  dragEnd(event) {
    document.getElementById('demo').innerHTML =
      'Finished dragging the p element.';
  }

  allowDrop(event) {
    event.preventDefault();
  }

  drop(event) {
    event.preventDefault();
    var data = event.dataTransfer.getData('Text');
    event.target.appendChild(document.getElementById(data));
  }
}
